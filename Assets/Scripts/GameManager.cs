﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private List<GameObject> enemies = new List<GameObject>();
    private GameObject[] enemiesFound;

    public GameObject enemyPrefab;
    public GameObject bulletPrefab;


 //   private bool bulletIsActive = false;

    public float bulletSpeed;
    private int shiplife=3;
    private int nEnemiesDefeated = 0;
    public Text shipLifes;
    public Text enemiesDefeated;
    private GameObject group;

    public int nEnemies=10;
    private Rigidbody rbBullet;
   

    public int NEnemiesDefeated { get => nEnemiesDefeated; set => nEnemiesDefeated = value; }

    private void Awake()
    {
       
        //Buscamos los aliens en escena
        enemiesFound = GameObject.FindGameObjectsWithTag("Alien");
        //Los pasamos a una lista para poder borrarlos.
        foreach (GameObject go in enemiesFound)
            enemies.Add(go);
      
        Debug.Log("Has derrotado a: "+NEnemiesDefeated);
        enemiesDefeated.text = NEnemiesDefeated + " / "+nEnemies +" aliens defeated.";
      
        group = GameObject.FindGameObjectWithTag("grupo");

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
        Debug.Log("Tienes "+ shiplife+"vidas");
        shipLifes.text = shiplife + " lifes remaining";
        
        Debug.Log("Has derrotado "+NEnemiesDefeated);  
        enemiesDefeated.text = NEnemiesDefeated + " / " + nEnemies + " aliens defeated.";

        //Si nos quitan las 3 vidas o derrotamos a todos los enemigos mostrará la escena final (para resetear o salir del juego)
        if (shiplife < 1 || NEnemiesDefeated == nEnemies)
            StartCoroutine("wait2Secs");
        
    }

    //Cargamos la escena siguiente buildeada
    public void FinishGame()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    IEnumerator wait2Secs()
    {
       
        yield return new WaitForSeconds(2f);
        FinishGame();

    }
}
  
