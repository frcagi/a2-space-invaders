﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactTrigger : MonoBehaviour
{
    private int shipLife=3;

    public int ShipLife { get => shipLife; set => shipLife = value; }

    private void OnTriggerEnter(Collider collision)
    {        
        if (collision.gameObject.tag.ToLower().Equals("alien"))
        {
            shipLife -= 1;
       
        }

    }
}
   
    
       
    




