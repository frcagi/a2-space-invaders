﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuScript : MonoBehaviour
{
 
  //Cargamos la escena anterior buildeada (en este caso la del juego)
   public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }
    //Salimos inmediatamente del juego. (En Unity no funciona)
    public void quitGame()
    {
        Application.Quit(0);

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }
}
