﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementGroup : MonoBehaviour
{

    public float alienSpeed;
    private bool moveLeft;

    public int nEnemies = 10;
    public GameObject enemyPrefab;
    private void Awake()
    {

        for (int i = 0; i < nEnemies; i++)
        {
            GameObject nuevoAlien = Instantiate(enemyPrefab, new Vector3(-28 + (i + 4f), 30, 0), Quaternion.identity);
            nuevoAlien.transform.parent = transform;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
      
        moveLeft = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Movement();
    }
    void Movement()
    {
        if (!moveLeft)
        {
            transform.Translate(Vector3.right * alienSpeed * Time.deltaTime);
          
            if (transform.position.x >= 28f)
            {
                transform.Translate(Vector3.down * (1.03f * alienSpeed) * Time.deltaTime);
                alienSpeed = alienSpeed + (1.3f * Time.deltaTime);
                moveLeft = true;


            }
        }
        else if (moveLeft)
        {
            transform.Translate(Vector3.left * alienSpeed * Time.deltaTime);

            if (transform.position.x <= -28f)
            {
                transform.Translate(Vector3.down * (1.03f * alienSpeed) * Time.deltaTime);
                alienSpeed = alienSpeed + (1.3f * Time.deltaTime);
                moveLeft = false;

            }

        }

    }
}