﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    Rigidbody rb;
    public float Speed;
    private bool isBulletActive;
    public GameObject bulletPrefab;
    private GameObject newBullet;    

    

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isBulletActive = false;
    }

    void FixedUpdate()
    {
        Movimiento();
    }
    void Movimiento()
    {
        // m = número entre (-1<=1)
        float m = Input.GetAxis("Horizontal");

        if (m != 0)
        {
            Vector3 movement = new Vector3(m, 0, 0);
            rb.velocity = movement * Speed;

            float posicion = Mathf.Clamp(rb.position.x, -28f, 28f);

            rb.position = new Vector3(posicion, 0f, 0f);
        }

        if (m == 0)
        {
            Vector3 movement = new Vector3(0, 0, 0);
            rb.velocity = movement * Speed;
        }
        //Disparamos con la espaciadora
        if (Input.GetAxis("Jump")>0)
        {
            //SOLO UNA BALA
            if (!isBulletActive) {
                newBullet = Instantiate(bulletPrefab,new Vector3(transform.position.x, transform.position.y+5f, transform.position.z), Quaternion.identity);
                isBulletActive = true;
                newBullet.GetComponent<Rigidbody>().velocity = Vector3.up * 50f;

            }
            else {
                //Cuando la destruimos es null así que cuando disparamos y sea null, volvemos a activarla
                if (newBullet == null)
                {
                    isBulletActive = false;
                }
                else
                {
                    Debug.Log("Only one shoot at time");
                }
                
                         
            }
        }
    }
   
}
